import { useQuery } from "@apollo/client";
import type { NextPage } from "next";
import Head from "next/head";
import { useEffect, useState } from "react";
import { GET_ANIME_LIST } from "../src/services/gql";
import Pagination from "../src/components/Home/Pagination";
import ListAnime from "../src/components/Home/ListAnime";
import { iAnilistPage, iPagination } from "../src/interfaces/page";
import { Container, mq } from "../src/styles";
import HeaderFront from "../src/components/HeaderFront";
import Layout from "../src/components/Layout";
import { iAnilistMedia } from "../src/interfaces/media";
import AddCollectionModal from "../src/components/Collection/AddCollectionModal";

const Home: NextPage = () => {
  const [page, setPage] = useState(1);
  const [showBulkAddModal, setShowBulkAddModal] = useState(false);
  const [bulkAdd, setBulkAdd] = useState<iAnilistMedia[]>([]);
  const [paginationData, setPaginationData] = useState<iPagination>({
    currentPage: 1,
    hasNextPage: true,
    lastPage: 1,
    perPage: 10,
    total: 10,
  });

  const animeData = useQuery<iAnilistPage>(GET_ANIME_LIST, {
    variables: {
      page,
      perPage: 10,
    },
  });

  useEffect(() => {
    if (animeData.data) {
      setPaginationData(animeData.data?.Page.pageInfo);
    }
  }, [animeData.data]);

  const handleBulkCheckBox = (data: iAnilistMedia) => {
    const found = bulkAdd.findIndex(
      (thisBulkData) => thisBulkData.id === data.id
    );
    if (found > -1) {
      const deletedBulkAdd = [...bulkAdd];
      deletedBulkAdd.splice(found, 1);
      setBulkAdd(deletedBulkAdd);
    } else {
      setBulkAdd([...bulkAdd, data]);
    }
  };

  const headerTitle = !!bulkAdd.length ? "+Add to Collection" : "";

  return (
    <div>
      <Head>
        <title>AnimeList</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <HeaderFront
          title={headerTitle}
          handleClickMiddleTitle={() => setShowBulkAddModal(true)}
        />
        <Container
          css={mq({
            display: "block",
            marginTop: ["90px", "90px", "50px"],
          })}
        >
          <ListAnime
            animeData={animeData}
            bulkData={bulkAdd}
            handleCheckbox={(data: iAnilistMedia) => handleBulkCheckBox(data)}
          />
        </Container>
        <Pagination
          {...paginationData}
          goToPage={setPage}
          loading={!animeData.data}
        />
        <AddCollectionModal
          show={showBulkAddModal}
          handleCloseModal={() => setShowBulkAddModal(false)}
          handleCallback={() => setBulkAdd([])}
          animeData={bulkAdd}
        />
      </Layout>
    </div>
  );
};

export default Home;
