import { useQuery } from "@apollo/client";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { GET_ANIME_DETAIL } from "../../src/services/gql";
import { iAnilistMediaDetailData } from "../../src/interfaces/media";
import Header from "../../src/components/Detail/Header";
import Content from "../../src/components/Detail/Content";
import AddCollectionModal from "../../src/components/Collection/AddCollectionModal";
import { useState } from "react";
import { getCollectionsByAnime } from "../../src/services/storage";
import CollectionList from "../../src/components/Detail/CollectionList";
import Announcer from "../../src/components/Announcer";
import Layout from "../../src/components/Layout";
import RelatedAnime from "../../src/components/Detail/RelatedAnime";

const Detail: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const [showAddCollectionModal, setShowAddCollectionModal] = useState(false);

  const { loading, error, data } = useQuery<iAnilistMediaDetailData>(
    GET_ANIME_DETAIL,
    {
      variables: {
        id,
      },
    }
  );

  if (loading) return <Announcer text="Loading..." />;
  if (error || !data) return <Announcer text="Error :(" />;

  const {
    coverImage,
    title,
    description,
    bannerImage,
    genres,
    episodes,
    averageScore,
    meanScore,
    status,
    startDate,
    id: idAnime,
    relations,
  } = data.Media;

  const HeaderProps = {
    coverImage,
    title,
    bannerImage,
    genres,
    description,
  };

  const contentProps = {
    episodes,
    averageScore,
    meanScore,
    status,
    startDate,
  };

  const animeData = {
    title,
    id: idAnime,
    coverImage,
  };

  const collectionListData = getCollectionsByAnime(animeData.id);

  return (
    <div>
      <Head>
        <title>{title.romaji} | AnimeList</title>
        <meta name="description" content={description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Layout>
        <Header
          {...HeaderProps}
          handleClickAddCollection={() => setShowAddCollectionModal(true)}
        />
        <Content {...contentProps} />
        {!!collectionListData.length && (
          <CollectionList collections={collectionListData} />
        )}
        <RelatedAnime animeData={relations} />
        <AddCollectionModal
          show={showAddCollectionModal}
          handleCloseModal={() => setShowAddCollectionModal(false)}
          animeData={[animeData]}
        />
      </Layout>
    </div>
  );
};

export default Detail;
