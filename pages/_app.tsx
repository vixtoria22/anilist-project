import type { AppProps } from "next/app";
import { client } from "../src/services/gql";
import { ApolloProvider } from "@apollo/client";
import { Global, css } from "@emotion/react";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <Global
        styles={css`
          body {
            margin: 0;
          }
          * {
            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Oxygen,
              Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
              sans-serif;
          }

          h1 {
            font-size: 1rem;
          }

          h2 {
            font-size: 0.8rem;
          }

          @media (min-width: 375px) {
            h1 {
              font-size: 1.1rem;
            }
            h2 {
              font-size: 1rem;
            }
          }

          @media (min-width: 800px) {
            h1 {
              font-size: 1.9rem;
            }
            h2 {
              font-size: 1.6rem;
            }
          }

        `}
      />
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default MyApp;
