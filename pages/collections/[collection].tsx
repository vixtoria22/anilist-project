import type { NextPage } from "next";
import Head from "next/head";
import { Container, mq } from "../../src/styles";
import HeaderFront from "../../src/components/HeaderFront";
import {
  getCollectionDetail,
  deleteAnimeFromCollection,
} from "../../src/services/storage";
import { useRouter } from "next/router";
import ConfirmModal from "../../src/components/Collection/ConfirmModal";
import { useState } from "react";
import ListAnimeCollection from "../../src/components/Collection/ListAnimeCollection";
import { iAnilistMedia, iTitleAnime } from "../../src/interfaces/media";
import Announcer from "../../src/components/Announcer";
import Layout from "../../src/components/Layout";
import EditCollectionModal from "../../src/components/Collection/EditCollectionModal";

interface iDeleteAnimeCollection {
  title: iTitleAnime;
  id: number;
}

const CollectionsDetail: NextPage = () => {
  const router = useRouter();
  const { collection } = router.query;

  const [showDeleteModal, setShowDeleteModal] = useState<
    iDeleteAnimeCollection | undefined
  >(undefined);

  const [showEditModal, setShowEditModal] = useState(false);

  let data: iAnilistMedia[] = [];

  if (global.window) {
    data = getCollectionDetail(`${collection}`);
  }

  const handleDeleteAnimeFromCollection = (value: boolean) => {
    if (value && showDeleteModal)
      deleteAnimeFromCollection(`${collection}`, showDeleteModal.id);
    setShowDeleteModal(undefined);
  };

  return (
    <div>
      <Head>
        <title>{collection} | Collections</title>
      </Head>
      <Layout>
        <HeaderFront
          title={`${collection}`}
          handleClickMiddleTitle={() => setShowEditModal(true)}
        />
        <Container
          css={mq({
            display: "block",
            marginTop: ["90px", "90px", "50px"],
          })}
        >
          {data && !data.length ? (
            <Announcer text="This collection is empty, please add anime." />
          ) : (
            <ListAnimeCollection
              data={data}
              handleDelete={(
                e: MouseEvent,
                thisData: iDeleteAnimeCollection
              ) => {
                e.preventDefault();
                setShowDeleteModal(thisData);
              }}
            />
          )}
        </Container>
        <ConfirmModal
          show={!!showDeleteModal}
          name={showDeleteModal ? showDeleteModal.title.romaji : ""}
          handleCloseModal={(value: boolean) =>
            handleDeleteAnimeFromCollection(value)
          }
        />
        <EditCollectionModal
          show={showEditModal}
          oldCollection={`${collection}`}
          handleCloseModal={() => setShowEditModal(false)}
          handleCallback={(value: string) =>
            router.replace({
              href: "/collections/[collection]",
              query: {
                collection: value,
              },
            })
          }
        />
      </Layout>
    </div>
  );
};

export default CollectionsDetail;
