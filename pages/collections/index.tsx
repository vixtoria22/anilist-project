import type { NextPage } from "next";
import Head from "next/head";
import { Container, mq } from "../../src/styles";
import HeaderFront from "../../src/components/HeaderFront";
import {
  deleteCollection,
  getCollectionList,
} from "../../src/services/storage";
import ListCollection from "../../src/components/Collection/ListCollection";
import NewCollectionModal from "../../src/components/Collection/NewCollectionModal";
import { useState } from "react";
import ConfirmModal from "../../src/components/Collection/ConfirmModal";
import Layout from "../../src/components/Layout";
import EditCollectionModal from "../../src/components/Collection/EditCollectionModal";

const CollectionsIndex: NextPage = () => {
  const [showNewCollectionModal, setShowNewCollectionModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState("");
  const [showEditModal, setShowEditModal] = useState("");

  const handleDeleteCollection = (value: boolean) => {
    if (value) deleteCollection(showDeleteModal);
    setShowDeleteModal("");
  };

  return (
    <div>
      <Head>
        <title>AnimeList Collections</title>
      </Head>
      <Layout>
        <HeaderFront
          handleClickMiddleTitle={() => setShowNewCollectionModal(true)}
        />
        <Container
          css={mq({
            display: "block",
            marginTop: ["90px", "90px", "50px"],
          })}
        >
          {global.window && (
            <ListCollection
              collections={getCollectionList()}
              triggerDeleteCollection={(value: string) =>
                setShowDeleteModal(value)
              }
              triggerEditCollection={(value: string) => setShowEditModal(value)}
            />
          )}
        </Container>
        <NewCollectionModal
          show={showNewCollectionModal}
          handleCloseModal={() => setShowNewCollectionModal(false)}
        />
        <EditCollectionModal
          show={!!showEditModal}
          oldCollection={showEditModal}
          handleCloseModal={() => setShowEditModal("")}
        />
        <ConfirmModal
          show={!!showDeleteModal}
          name={showDeleteModal}
          handleCloseModal={(value: boolean) => handleDeleteCollection(value)}
        />
      </Layout>
    </div>
  );
};

export default CollectionsIndex;
