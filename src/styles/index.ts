import { css } from "@emotion/react";
import styled from "@emotion/styled";
import facepaint from "facepaint";

const breakpoints = [375, 768];

export const mq = facepaint(
  breakpoints.map((bp) => `@media (min-width: ${bp}px)`)
);

const marginVertical = ["0", "0", "20px"];

const BaseButtons = css(
  mq({
    alignItems: "center",
    borderRadius: "3px",
    color: "white",
    cursor: "pointer",
    display: "flex",
    fontSize: ["1rem", "1rem", "1.4rem"],
    height: "35px",
    justifyContent: "center",
    lineHeight: "1.3rem",
    width: "100%",
    border: "1px white solid",
    marginTop: "5px",
    marginBottom: "20px",
  })
);

export const Container = styled.div({
  display: "grid",
  gridColumnGap: "30px",
  gridTemplateColumns: "215px auto",
  margin: "20px",
  minWidth: "320px",
});

export const FlexContainer = styled.div(
  mq({
    display: "flex",
    justifyContent: "space-around",
    flexDirection: "row",
    marginRight: marginVertical,
    marginLeft: marginVertical,
    padding: ["5px", "5px", "10px"],
    borderRadius: "5px",
    boxShadow:
      "0 14px 30px rgba(103,132,187,.15), 0 14px 30px rgba(103,132,187,.15)",
  })
);

export const CoverWrapper = styled.div(
  mq({
    background: "rgba(221, 230, 238, 0.8)",
    borderRadius: "4px",
    boxShadow:
      "0 14px 30px rgba(103, 132, 187, 0.15), 0 4px 4px rgba(103, 132, 187, 0.05)",
    cursor: "pointer",
    display: "inline-block",
    height: ["200px", "200px", "265px"],
    overflow: "hidden",
    position: "relative",
    width: "100%",
  })
);

export const ButtonPrimary = styled.div([
  BaseButtons,
  {
    background: "#A66CFF",
    ":hover": {
      background: "#A66CAA",
    },
  },
]);

export const ButtonSecondary = styled.div([
  BaseButtons,
  {
    background: "#EAEAEA",
    color: "#333333",
  },
]);
