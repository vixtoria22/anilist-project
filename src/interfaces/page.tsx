import { iAnilistMedia } from "./media";

export interface iPagination {
  currentPage: number;
  hasNextPage: boolean;
  lastPage: number;
  perPage: number;
  total: number;
}

export interface iAnilistPage {
  Page: {
    media: iAnilistMedia[];
    pageInfo: iPagination;
  };
}
