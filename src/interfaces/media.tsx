export interface iTitleAnime {
  romaji: string;
  english: string;
  native: string;
}

export interface iDate {
  day: string;
  month: string;
  year: string;
}

export interface iCoverImageAnime {
  medium: string;
  large: string;
  extraLarge: string;
}

export interface iAnilistMedia {
  title: iTitleAnime;
  id: number;
  coverImage: iCoverImageAnime;
}

export interface iEdgeRelation {
  id: number;
  node: iAnilistMedia;
}

export interface iAnilistMediaDetail extends iAnilistMedia {
  bannerImage: string;
  description: string;
  episodes: number;
  status: string;
  averageScore: string;
  meanScore: string;
  startDate: iDate;
  genres: string[];
  relations: {
    edges: iEdgeRelation[];
  };
}

export interface iAnilistMediaDetailData {
  Media: iAnilistMediaDetail;
}

export interface iListAnimeLocal {
  data?: iAnilistMedia[];
  handleDelete: Function;
}
