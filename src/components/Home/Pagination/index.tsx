import { iPagination } from "../../../interfaces/page";
import { FlexContainer } from "../../../styles";
import { ButtonPagination } from "./index.style";

const BtnPagination = ButtonPagination.withComponent("button");

interface iCPagination extends iPagination {
  goToPage: Function;
  loading: boolean;
}

const Pagination = ({
  lastPage,
  hasNextPage,
  currentPage,
  goToPage,
  loading,
}: iCPagination) => (
  <FlexContainer
    css={{
      justifyContent: "center",
      boxShadow: "none",
    }}
  >
    <BtnPagination
      disabled={loading || currentPage < 1}
      onClick={() => goToPage(1)}
    >
      &lt;&lt;
    </BtnPagination>
    <BtnPagination
      disabled={loading || currentPage < 1}
      onClick={() => goToPage(currentPage - 1)}
    >
      &lt;
    </BtnPagination>
    <BtnPagination disabled>
      {currentPage} of {lastPage}
    </BtnPagination>
    <BtnPagination
      disabled={loading || !hasNextPage}
      onClick={() => goToPage(currentPage + 1)}
    >
      &gt;
    </BtnPagination>
    <BtnPagination
      disabled={loading || !hasNextPage}
      onClick={() => goToPage(lastPage)}
    >
      &gt;&gt;
    </BtnPagination>
  </FlexContainer>
);

export default Pagination;
