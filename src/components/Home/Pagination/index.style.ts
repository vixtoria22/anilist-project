import styled from "@emotion/styled";
import { mq } from "../../../styles";

export const ButtonPagination = styled.div(
  mq({
    alignItems: "center",
    background: "#1C3879",
    borderRadius: "5px",
    color: "white",
    margin: "2px",
    cursor: "pointer",
    display: "flex",
    fontSize: ["1rem", "1rem", "1.4rem"],
    border: "none",
    fontWeight: 100,
    height: "35px",
    justifyContent: "center",
    paddingLeft: "1px",
    transition: ".2s",
    padding: "0 14px",
    ":hover": {
      background: "#A66CAA",
    },
    ":disabled": {
      cursor: "default",
      background: "grey",
    },
  })
);
