import { QueryResult } from "@apollo/client";
import Image from "next/image";
import Link from "next/link";
import { iAnilistMedia } from "../../../interfaces/media";
import { iAnilistPage } from "../../../interfaces/page";
import { CoverWrapper } from "../../../styles";
import Announcer from "../../Announcer";
import ListShimmer from "../ListShimmer";
import { Grid, Card, BolderText, ImageStyled } from "./index.style";

const BolderH3 = BolderText.withComponent("h3");
const ImageCover = ImageStyled.withComponent(Image);

interface iCListAnime {
  animeData: QueryResult<iAnilistPage>;
  bulkData: iAnilistMedia[];
  handleCheckbox: Function;
}

const ListAnime = ({ bulkData, animeData, handleCheckbox }: iCListAnime) => {
  const { data, loading, error } = animeData;

  if (loading) return <ListShimmer />;
  if (error) return <Announcer text="Error :(" />;

  return (
    <Grid>
      {data &&
        data.Page.media.map(({ id, title, coverImage }: iAnilistMedia) => (
          <Card key={`anime_${id}`}>
            <input
              type="checkbox"
              css={{
                position: "absolute",
                top: 0,
                right: 0,
                zIndex: 10,
                height: 20,
                width: 20,
              }}
              name="anime"
              readOnly
              value={id}
              checked={!!bulkData.find((thisData) => thisData.id === id)}
              onClick={() => {
                handleCheckbox({ id, title, coverImage });
              }}
            />
            <Link
              href={{
                pathname: "/anime/[id]",
                query: {
                  id,
                },
              }}
            >
              <div>
                <CoverWrapper>
                  <ImageCover
                    layout="fill"
                    alt={`Cover ${title.romaji}`}
                    src={coverImage.large}
                  />
                </CoverWrapper>
                <BolderH3>{title.romaji}</BolderH3>
              </div>
            </Link>
          </Card>
        ))}
    </Grid>
  );
};

export default ListAnime;
