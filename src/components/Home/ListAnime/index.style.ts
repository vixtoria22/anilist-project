import { keyframes } from "@emotion/react";
import styled from "@emotion/styled";
import { mq } from "../../../styles";

const scaled = keyframes({
  "0%": { opacity: 0, transform: "scale(0.92)" },
  "60%": { opacity: 1 },
  to: { transform: "none" },
});

export const Grid = styled.div(
  mq({
    display: "grid",
    gridGap: ["10px 5px", "20px 10px", "25px 30px"],
    gridTemplateColumns: [
      "repeat(auto-fill, 150px)",
      "repeat(auto-fill, 160px)",
      "repeat(auto-fill, 170px)",
    ],
    justifyContent: "space-between",
  })
);

export const Card = styled.div({
  animation: `${scaled} 0.3s linear`,
  display: "grid",
  gridTemplateRows: "min-content auto",
  position: "relative",
  width: "100%",
  height: "auto",
});

export const BolderText = styled.div({
  cursor: "pointer",
  color: "#060606",
  fontSize: "1rem",
  fontWeight: 500,
  marginTop: "10px",
  overflow: "hidden",
  minHeight: "2.4rem",
  maxHeight: "2.4rem",
  textOverflow: "ellipsis",
  display: "-webkit-box",
  "-webkit-line-clamp": "2",
  "-webkit-box-orient": "vertical",
});

export const ImageStyled = styled.div({
  height: "100%",
  left: "0",
  objectFit: "cover",
  transition: "opacity 0.3s ease-in-out",
  position: "absolute",
  width: "100%",
});
