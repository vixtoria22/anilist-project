import { CoverWrapper } from "../../../styles";
import { Grid, Card } from "../ListAnime/index.style";


const ListShimmer = () => {
  return (
    <Grid>
      {Array(10)
        .fill(1)
        .map((_, index) => (
          <Card key={`shimmer_${index}`} role="card">
            <div>
              <CoverWrapper />
              <div
                css={{
                  background: "rgba(221, 230, 238, 0.8)",
                  height: "2.15rem",
                  borderRadius: "4px",
                  boxShadow:
                    "0 14px 30px rgb(103 132 187 / 15%), 0 4px 4px rgb(103 132 187 / 5%)",
                  marginTop: "10px",
                  marginBottom: "20px",
                }}
              />
            </div>
          </Card>
        ))}
    </Grid>
  );
};

export default ListShimmer;
