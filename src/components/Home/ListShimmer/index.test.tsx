import { render, screen } from "@testing-library/react";
import ListShimmer from ".";

describe("ListShimmer", () => {
  it("renders 10 cards", () => {
    render(<ListShimmer />);

    const cards = screen.getAllByRole("card");

    expect(cards).toHaveLength(10);
  });
});
