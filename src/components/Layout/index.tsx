import { ReactNode } from "react";
import { mq } from "../../styles";

const Layout = ({ children }: { children: ReactNode }) => (
  <main
    css={mq({
      margin: "0 auto",
      marginBottom: "50px",
      maxWidth: ["360px", "768px", "1024px"],
    })}
  >
    {children}
  </main>
);

export default Layout;
