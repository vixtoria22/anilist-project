import Image from "next/image";
import Link from "next/link";
import { iAnilistMedia, iEdgeRelation } from "../../../interfaces/media";
import { CoverWrapper, mq } from "../../../styles";
import {
  Grid,
  Card,
  BolderText,
  ImageStyled,
} from "../../Home/ListAnime/index.style";

const BolderH3 = BolderText.withComponent("h3");
const ImageCover = ImageStyled.withComponent(Image);

interface iRelatedAnime {
  animeData: {
    edges: iEdgeRelation[];
  };
}

const RelatedAnime = ({ animeData }: iRelatedAnime) => {
  return (
    <div
      css={mq({
        marginLeft: ["20px", "20px", "5px"],
        marginRight: ["20px", "20px", "5px"],
      })}
    >
      <h3>Related anime:</h3>
      <Grid>
        {animeData.edges.map(
          ({ node: { id, title, coverImage } }: iEdgeRelation) => (
            <Card key={`anime_${id}`}>
              <Link
                href={{
                  pathname: "/anime/[id]",
                  query: {
                    id,
                  },
                }}
              >
                <div>
                  <CoverWrapper>
                    <ImageCover
                      layout="fill"
                      alt={`Cover ${title.romaji}`}
                      src={coverImage.large}
                    />
                  </CoverWrapper>
                  <BolderH3>{title.romaji}</BolderH3>
                </div>
              </Link>
            </Card>
          )
        )}
      </Grid>
    </div>
  );
};

export default RelatedAnime;
