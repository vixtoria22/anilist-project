import Image from "next/image";

const ButtonBack = (props: any) => (
  <button
    {...props}
    css={{
      position: "absolute",
      cursor: "pointer",
      zIndex: 10,
      left: 0,
      paddingTop: "10px",
      paddingBottom: "10px",
      paddingRight: "20px",
      paddingLeft: "20px",
      background: "transparent",
      fontSize: "1.5em",
      boxShadow:
        "0 14px 30px rgba(103,132,187,.05), 0 14px 30px rgba(103,132,187,.05)",
      border: 0,
      ":hover": {
        background: "rgba(0,0,0, 40%)",
        color: "#fff",
      },
    }}
    onClick={() => window.history.back()}
  >
    <Image src="/back.svg" width={20} height={20} alt="button back" />
  </button>
);

export default ButtonBack;
