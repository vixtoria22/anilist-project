import styled from "@emotion/styled";
import { mq } from "../../../styles";

export const Banner = styled.div(
  mq({
    display: "inline-block",
    overflow: "hidden",
    position: "relative",
    height: ["200px","230px", "400px"],
    width: "100%",
  })
);

export const OverlapBanner = styled.div({
  marginTop: "-125px",
  position: "relative",
});

export const Content = styled.div({
  display: "inline-grid",
  gridTemplateRows: "min-content min-content auto",
});

export const GenrePill = styled.div(
  mq({
    background: "#5FB8FF",
    borderRadius: "10px",
    color: "white",
    display: ["block", "block", "inline-block"],
    fontSize: "1.1rem",
    opacity: "0.8",
    fontWeight: 300,
    height: "18px",
    lineHeight: "1rem",
    marginRight: "8px",
    marginBottom: "5px",
    padding: "0 10px",
    textTransform: "lowercase",
  })
);