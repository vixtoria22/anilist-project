import Image from "next/image";
import { iCoverImageAnime, iTitleAnime } from "../../../interfaces/media";
import { ButtonPrimary, Container, CoverWrapper, mq } from "../../../styles";
import ButtonBack from "../ButtonBack";
import { Banner, OverlapBanner, Content, GenrePill } from "./index.style";

interface iCDetailHeader {
  bannerImage: string;
  description: string;
  coverImage: iCoverImageAnime;
  genres: string[];
  title: iTitleAnime;
  handleClickAddCollection: VoidFunction;
}

const ButtonAddToCollection = ButtonPrimary.withComponent("button");

const Header = ({
  bannerImage,
  title,
  coverImage,
  description,
  genres,
  handleClickAddCollection,
}: iCDetailHeader) => (
  <div>
    <Banner>
      <ButtonBack
        css={mq({
          display: ["block", "block", "none"],
        })}
      />
      <Image
        src={bannerImage || "/404.png"}
        layout="fill"
        css={{ zIndex: "-1" }}
        alt={`Banner ${title.romaji}`}
        objectFit="cover"
        objectPosition="center"
      />
      <div
        css={{
          bottom: "5px",
          position: "absolute",
          right: "0",
        }}
      >
        {genres.map((genre) => (
          <GenrePill key={genre}>{genre}</GenrePill>
        ))}
      </div>
    </Banner>

    <Container
      css={mq({
        minHeight: "250px",
        display: ["block", "block", "grid"],
      })}
    >
      <OverlapBanner css={mq({ width: ["180px", "180px", "100%"] })}>
        <CoverWrapper>
          <Image
            alt={`Cover ${title.romaji}`}
            src={coverImage.extraLarge || "/404.png"}
            css={{ borderRadius: "2px" }}
            layout="fill"
          />
        </CoverWrapper>
        <ButtonAddToCollection onClick={handleClickAddCollection}>
          Add to Collection
        </ButtonAddToCollection>
      </OverlapBanner>
      <Content>
        <h1>{title.romaji}</h1>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </Content>
    </Container>
  </div>
);

export default Header;
