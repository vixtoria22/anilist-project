import { iDate } from "../../../interfaces/media";
import { FlexContainer, mq } from "../../../styles";

interface iCDetailContent {
  episodes: number;
  averageScore: string;
  meanScore: string;
  status: string;
  startDate: iDate;
}

const Content = ({
  episodes,
  averageScore,
  meanScore,
  status,
  startDate,
}: iCDetailContent) => (
  <div>
    <FlexContainer
      css={mq({
        flexDirection: ["column", "column", "row"],
        gap: "10px",
        marginLeft: ["20px", "20px", "5px"],
        marginRight: ["20px", "20px", "5px"],
      })}
    >
      <div>Status: {status}</div>
      <div>
        Release Date: {startDate.day}/{startDate.month}/{startDate.year}
      </div>
      <div>
        Episode{episodes > 1 ? "s" : ""}: {episodes}
      </div>
      <div>Average Score: {averageScore}%</div>
      <div>Mean Score: {meanScore}%</div>
    </FlexContainer>
  </div>
);

export default Content;
