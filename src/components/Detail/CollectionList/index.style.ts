import styled from "@emotion/styled";

export const CollectionPill = styled.div({
  alignItems: "center",
  background: "#5881A2",
  borderRadius: "5px",
  color: "white",
  margin: "2px",
  cursor: "pointer",
  display: "flex",
  fontSize: "1rem",
  border: "none",
  fontWeight: 250,
  height: "25px",
  justifyContent: "center",
  paddingLeft: "1px",
  transition: ".2s",
  padding: "0 14px",
  ":hover": {
    background: "#A66CAA",
  },
});