import Link from "next/link";
import { FlexContainer, mq } from "../../../styles";
import { CollectionPill } from "./index.style";

const CollectionList = ({ collections }: { collections: string[] }) => (
  <div css={{ marginTop: "20px" }}>
    <h4 css={{ marginLeft: "20px", marginRight: "20px" }}>
      Added to Your Collection{collections.length > 1 && "s"}:
    </h4>
    <FlexContainer
      css={mq({
        justifyContent: "flex-start",
        marginLeft: ["20px", "20px", "5px"],
        marginRight: ["20px", "20px", "5px"],
      })}
    >
      {collections.map((thisCollection) => (
        <Link
          key={thisCollection}
          href={{
            pathname: "/collections/[collection]",
            query: {
              collection: thisCollection,
            },
          }}
        >
          <CollectionPill>{thisCollection}</CollectionPill>
        </Link>
      ))}
    </FlexContainer>
  </div>
);

export default CollectionList;
