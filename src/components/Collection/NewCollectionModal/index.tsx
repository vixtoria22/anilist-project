import { useState } from "react";
import { addNewCollection } from "../../../services/storage";
import { ButtonPrimary, ButtonSecondary, mq } from "../../../styles";
import Modal from "../../Modal";
import NewCollectionForm from "../NewCollectionForm";

interface iCNewCollectionModal {
  show: boolean;
  handleCloseModal: VoidFunction;
}

const ButtonConfirm = ButtonPrimary.withComponent("button");
const ButtonCancel = ButtonSecondary.withComponent("button");

const NewCollectionModal = ({ show, handleCloseModal }: iCNewCollectionModal) => {
  const [newCollection, setNewCollection] = useState("");

  const handleAddNewCollection = () => {
    try {
      addNewCollection(newCollection);
      setNewCollection("");
    } catch (e) {
      window.alert(e);
    } finally {
      handleCloseModal();
    }
  };
  return (
    <Modal
      open={show}
      onClose={() => handleCloseModal()}
    >
      <form
        css={mq({
          display: "grid",
          gridGap: "1em",
          justifyItems: "center",
          padding: ["0.5em", "0.5em", "1em"],
        })}
        method="dialog"
        onSubmit={handleAddNewCollection}
      >
        <NewCollectionForm
          setNewCollection={setNewCollection}
          newCollection={newCollection}
        />
        <div css={{ display: "inline-flex" }}>
          <ButtonCancel
            onClick={handleCloseModal}
            type="reset"
            css={{ marginRight: "15px", marginBottom: "0" }}
          >
            Cancel
          </ButtonCancel>
          <ButtonConfirm type="submit" css={{ marginBottom: "0" }}>
            Add
          </ButtonConfirm>
        </div>
      </form>
    </Modal>
  );
};

export default NewCollectionModal;
