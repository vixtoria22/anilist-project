import Image from "next/image";

const ButtonEdit = (props: any) => (
  <button
    {...props}
    css={{
      position: "absolute",
      cursor: "pointer",
      zIndex: 10,
      right: 0,
      bottom: 0,
      background: "rgba(255,255,255, 70%)",
      fontSize: "1.5em",
      border: 0,
      ":hover": {
        background: "rgba(234,234,234, 70%)",
        color: "#fff",
      },
    }}
  >
    <Image src="/edit.svg" width={20} height={20} alt="edit button" />
  </button>
);

export default ButtonEdit;
