import { useEffect, useState } from "react";
import { editCollection } from "../../../services/storage";
import { ButtonPrimary, ButtonSecondary, mq } from "../../../styles";
import Modal from "../../Modal";

interface iCEditModal {
  show: boolean;
  oldCollection: string;
  handleCloseModal: VoidFunction;
  handleCallback?: Function;
}

const ButtonConfirm = ButtonPrimary.withComponent("button");
const ButtonCancel = ButtonSecondary.withComponent("button");

const EditCollectionModal = ({
  show,
  handleCloseModal,
  oldCollection,
  handleCallback = () => {},
}: iCEditModal) => {
  const [newCollection, setNewCollection] = useState("");

  useEffect(() => {
    setNewCollection(oldCollection);
  }, [oldCollection])

  const handleEditCollection = () => {
    try {
      editCollection(oldCollection, newCollection);
      setNewCollection("");
      handleCallback(newCollection);
    } catch (e) {
      window.alert(e);
    } finally {
      handleCloseModal();
    }
  };
  return (
    <Modal
      open={show}
      onClose={() => handleCloseModal()}
    >
      <form
        css={mq({
          display: "grid",
          gridGap: "1em",
          justifyItems: "center",
          padding: ["0.5em", "0.5em", "1em"],
        })}
        method="dialog"
        onSubmit={handleEditCollection}
      >
        <label
          css={mq({
            fontSize: ["1em", "1.2em", "1.5em"],
            textAlign: "center",
          })}
        >
          Edit Collection:
          <input
            type="text"
            key="edit-collection"
            onChange={(e) => {
              e.preventDefault();
              setNewCollection(e.target.value);
            }}
            css={mq({ marginLeft: "10px", fontSize: "1em", marginTop: ["20px", "20px", 0] })}
            value={newCollection}
          />
        </label>
        <div css={{ display: "inline-flex" }}>
          <ButtonCancel
            onClick={handleCloseModal}
            type="reset"
            css={{ marginRight: "15px", marginBottom: "0" }}
          >
            Cancel
          </ButtonCancel>
          <ButtonConfirm type="submit" css={{ marginBottom: "0" }}>
            Update
          </ButtonConfirm>
        </div>
      </form>
    </Modal>
  );
};

export default EditCollectionModal;
