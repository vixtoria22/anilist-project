import { ButtonPrimary, ButtonSecondary } from "../../../styles";
import Modal from "../../Modal";

interface iConfirmModal {
  show: boolean;
  handleCloseModal: Function;
  name: string;
}

const ButtonConfirm = ButtonPrimary.withComponent("button");
const ButtonCancel = ButtonSecondary.withComponent("button");

const ConfirmModal = ({
  show,
  handleCloseModal,
  name,
}: iConfirmModal) => {
  return (
    <Modal
      open={show}
      onClose={() => handleCloseModal(false)}
    >
      <div
        css={{
          display: "grid",
          gridGap: "1em",
          justifyItems: "center",
          padding: "1em",
        }}
      >
        <h3>Are you sure to remove {name}?</h3>
        <div css={{ display: "inline-flex" }}>
          <ButtonCancel
            onClick={() => handleCloseModal(false)}
            type="reset"
            css={{ marginRight: "15px", marginBottom: "0" }}
          >
            Cancel
          </ButtonCancel>
          <ButtonConfirm
            onClick={() => handleCloseModal(true)}
            css={{ marginBottom: "0" }}
          >
            Confirm
          </ButtonConfirm>
        </div>
      </div>
    </Modal>
  );
};

export default ConfirmModal;
