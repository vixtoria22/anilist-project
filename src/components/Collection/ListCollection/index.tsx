import Image from "next/image";
import Link from "next/link";
import { iAnilistMedia } from "../../../interfaces/media";
import { getCollectionDetail } from "../../../services/storage";
import { CoverWrapper } from "../../../styles";
import Announcer from "../../Announcer";
import {
  Grid,
  Card,
  BolderText,
  ImageStyled,
} from "../../Home/ListAnime/index.style";
import ButtonClose from "../ButtonClose";
import ButtonEdit from "../ButtonEdit";

const BolderH3 = BolderText.withComponent("h3");
const ImageCover = ImageStyled.withComponent(Image);

const ListCollection = ({
  collections,
  triggerDeleteCollection,
  triggerEditCollection,
}: {
  collections: string[];
  triggerDeleteCollection: Function;
  triggerEditCollection: Function;
}) => {
  const listCovers = collections.map(
    (thisCollection) => getCollectionDetail(thisCollection)[0]
  );

  const handleShowDeleteCollection = (e: MouseEvent, collection: string) => {
    e.preventDefault();
    triggerDeleteCollection(collection);
  };

  const handleShowEditCollection = (e: MouseEvent, collection: string) => {
    e.preventDefault();
    triggerEditCollection(collection);
  };

  if (!listCovers.length) {
    return <Announcer text="No collections yet." />;
  }

  return (
    <Grid>
      {listCovers.map((data: iAnilistMedia, index) => {
        return (
          <Link
            key={collections[index]}
            href={{
              pathname: "/collections/[collection]",
              query: {
                collection: collections[index],
              },
            }}
          >
            <Card>
              <CoverWrapper>
                <ButtonClose
                  onClick={(e: MouseEvent) =>
                    handleShowDeleteCollection(e, collections[index])
                  }
                />
                <ButtonEdit
                  onClick={(e: MouseEvent) =>
                    handleShowEditCollection(e, collections[index])
                  }
                />
                <ImageCover
                  layout="fill"
                  alt={`Cover ${collections[index]}`}
                  src={data?.coverImage.large || "/404.png"}
                />
              </CoverWrapper>
              <BolderH3>{collections[index]}</BolderH3>
            </Card>
          </Link>
        );
      })}
    </Grid>
  );
};

export default ListCollection;
