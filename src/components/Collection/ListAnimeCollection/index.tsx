import Image from "next/image";
import Link from "next/link";
import { iAnilistMedia, iListAnimeLocal } from "../../../interfaces/media";
import { CoverWrapper } from "../../../styles";
import ButtonClose from "../../Collection/ButtonClose";
import {
  Grid,
  Card,
  BolderText,
  ImageStyled,
} from "../../Home/ListAnime/index.style";

const BolderH3 = BolderText.withComponent("h3");
const ImageCover = ImageStyled.withComponent(Image);

const ListAnimeCollection = ({ data, handleDelete }: iListAnimeLocal) => {
  return (
    <Grid>
      {data &&
        data.map(({ id, title, coverImage }: iAnilistMedia) => (
          <Link
            key={id}
            href={{
              pathname: "/anime/[id]",
              query: {
                id,
              },
            }}
          >
            <Card>
              <CoverWrapper>
                <ButtonClose
                  onClick={(e: MouseEvent) =>
                    handleDelete(e, {
                      id,
                      title,
                    })
                  }
                />
                <ImageCover
                  layout="fill"
                  alt={`Cover ${title.romaji}`}
                  src={coverImage.large}
                />
              </CoverWrapper>
              <BolderH3>{title.romaji}</BolderH3>
            </Card>
          </Link>
        ))}
    </Grid>
  );
};

export default ListAnimeCollection;
