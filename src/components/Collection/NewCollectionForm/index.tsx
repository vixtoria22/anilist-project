import { mq } from "../../../styles";

interface iCNewCollectionForm {
  setNewCollection: Function;
  newCollection: string;
  withAdd?: boolean;
}

const NewCollectionForm = ({
  setNewCollection,
  newCollection,
  withAdd = false,
}: iCNewCollectionForm) => (
  <label
    css={mq({
      fontSize: ["1em", "1.2em", "1.5em"],
      textAlign: "center",
    })}
  >
    {withAdd && "Add to "}New Collection:
    <input
      type="text"
      key="new-collection"
      onChange={(e) => {
        e.preventDefault();
        setNewCollection(e.target.value);
      }}
      css={mq({
        marginLeft: "10px",
        fontSize: "1em",
        marginTop: ["10px", "10px", "0"],
      })}
      value={newCollection}
    />
  </label>
);

export default NewCollectionForm;
