const ButtonClose = (props: any) => (
  <button
    {...props}
    css={{
      position: "absolute",
      cursor: "pointer",
      zIndex: 10,
      right: 0,
      background: "rgba(255,255,255, 70%)",
      fontSize: "1.5em",
      border: 0,
      ":hover": {
        background: "rgba(0,0,0, 80%)",
        color: "#fff",
      },
    }}
  >
    &times;
  </button>
);

export default ButtonClose;
