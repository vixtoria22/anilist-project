import { useEffect, useState } from "react";
import { iAnilistMedia } from "../../../interfaces/media";
import {
  addNewCollection,
  addToCollection,
  getCollectionList,
} from "../../../services/storage";
import { ButtonPrimary, ButtonSecondary, mq } from "../../../styles";
import Modal from "../../Modal";
import NewCollectionForm from "../NewCollectionForm";

interface iCAddCollectionModal {
  show: boolean;
  handleCloseModal: VoidFunction;
  handleCallback?: VoidFunction;
  animeData: iAnilistMedia[];
}

const ButtonConfirm = ButtonPrimary.withComponent("button");
const ButtonCancel = ButtonSecondary.withComponent("button");

const AddCollectionModal = ({
  show,
  handleCloseModal,
  animeData,
  handleCallback = () => {},
}: iCAddCollectionModal) => {
  const [collectionList, setCollectionList] = useState<string[]>([]);
  const [newCollection, setNewCollection] = useState("");
  const [selectedCollection, setSelectedCollection] = useState("");

  useEffect(() => {
    setCollectionList(getCollectionList());
  }, [show]);

  const RenderCollectionList = () => (
    <label
      css={mq({
        textAlign: "center",
        fontSize: "1.5em",
      })}
    >
      Add to Collection:
      <select
        css={mq({
          marginLeft: "5px",
          textAlign: "center",
          fontSize: "1em",
          marginTop: ["10px", "10px", "0"],
        })}
        onChange={(e) => setSelectedCollection(e.target.value)}
        value={selectedCollection}
      >
        <option value="default disabled">Choose…</option>
        {collectionList.map((thisCollection) => (
          <option key={thisCollection}>{thisCollection}</option>
        ))}
      </select>
    </label>
  );

  const handleAddNewCollection = () => {
    try {
      addNewCollection(newCollection);
      addToCollection(newCollection, animeData);
      setNewCollection("");
      handleCallback();
      window.alert("Anime successfully added to New Collection");
    } catch (e) {
      window.alert(e);
    } finally {
      handleCloseModal();
    }
  };

  const handleAddToCollection = () => {
    try {
      addToCollection(selectedCollection, animeData);
      setSelectedCollection("");
      window.alert("Anime successfully added to Collection");
      handleCallback();
    } catch (e) {
      window.alert(e);
    } finally {
      handleCloseModal();
    }
  };

  return (
    <Modal
      open={show}
      onClose={handleCloseModal}
    >
      <form
        css={{
          display: "grid",
          gridGap: "1em",
          justifyItems: "center",
          padding: "1em",
        }}
        method="dialog"
        onSubmit={
          collectionList.length ? handleAddToCollection : handleAddNewCollection
        }
      >
        {collectionList.length ? (
          <RenderCollectionList />
        ) : (
          <NewCollectionForm
            setNewCollection={setNewCollection}
            newCollection={newCollection}
            withAdd
          />
        )}
        <div css={{ display: "inline-flex" }}>
          <ButtonCancel
            onClick={handleCloseModal}
            type="reset"
            css={{ marginRight: "15px", marginBottom: "0" }}
          >
            Cancel
          </ButtonCancel>
          <ButtonConfirm type="submit" css={{ marginBottom: "0" }}>
            Add
          </ButtonConfirm>
        </div>
      </form>
    </Modal>
  );
};

export default AddCollectionModal;
