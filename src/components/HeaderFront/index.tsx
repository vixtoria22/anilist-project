import { css } from "@emotion/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { FlexContainer, mq } from "../../styles";

const activeColor = "#cccccc";
const hoverColor = "#eaeaff";

const baseHeaderAction = css(
  mq({
    cursor: "pointer",
    color: "blue",
    textShadow: "2px 2px lightblue",
    padding: ["3px", "3px", "5px"],
    borderRadius: "5px",
    whiteSpace: "nowrap",
    border: "2px solid",
    ":hover": {
      textShadow: "2px 2px cyan",
      background: hoverColor,
    },
  })
);

interface iCHeaderFront {
  handleClickMiddleTitle?: VoidFunction;
  title?: string;
}

const HeaderFront = ({
  handleClickMiddleTitle = () => {},
  title = "",
}: iCHeaderFront) => {
  const router = useRouter();
  const currentRoute = router.pathname;

  return (
    <FlexContainer
      css={mq({
        justifyContent: "space-between",
        alignItems: "center",
        position: ["fixed", "fixed", "relative"],
        top: "0",
        left: "0",
        right: "0",
        padding: ["0", "0", "5px"],
        minHeight: "8%",
        zIndex: 12,
        background: "#fff",
      })}
    >
      <Link
        href={{
          pathname: "/",
        }}
      >
        <h1
          css={{
            margin: "5px",
            cursor: "pointer",
            borderRadius: "5px",
            marginLeft: "20px",
            padding: "2px",
            border: '2px solid',
            background: currentRoute === "/" ? activeColor : "",
            ":hover": {
              background: hoverColor,
            },
          }}
        >
          AnimeList
        </h1>
      </Link>
      {title && (
        <h2
          onClick={handleClickMiddleTitle}
          css={[
            baseHeaderAction,
            {
              overflow: "hidden",
              textOverflow: "ellipsis",
            },
          ]}
        >
          {title}
        </h2>
      )}
      {currentRoute === "/collections" && (
        <h2 onClick={handleClickMiddleTitle} css={baseHeaderAction}>
          &#43;New Collection
        </h2>
      )}
      <Link
        href={{
          pathname: "/collections",
        }}
      >
        <h2
          css={[
            baseHeaderAction,
            {
              background: currentRoute === "/collections" ? activeColor : "",
              color: "lightcoral",
              marginRight: "20px",
            },
          ]}
        >
          &#9829;Collections
        </h2>
      </Link>
    </FlexContainer>
  );
};

export default HeaderFront;
