import { mq } from "../../styles";

const Announcer = ({ text }: { text: string }) => (
  <div
    css={mq({
      display: "flex",
      margin: "0 auto",
      justifyContent: "center",
      alignItems: "center",
      padding: ["40px", "50px", "60px"],
    })}
  >
    <p css={mq({ textAlign: "center", fontSize: ["1.4em", "1.5em", "3em"] })}>
      {text}
    </p>
  </div>
);

export default Announcer;
