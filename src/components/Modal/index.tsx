import { ReactElement, useEffect } from "react";
import { mq } from "../../styles";

interface iCModal {
  onClose: Function;
  children: ReactElement;
  open: boolean;
}

const Modal = ({ onClose, children, open }: iCModal) => {
  return (
    <div
      css={{
        position: "fixed",
        zIndex: 15,
        left: "0",
        top: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        transition: "all .1s linear",
        visibility: `${open ? "visible" : "hidden"}`,
        opacity: `${open ? "1" : "0"}`,
      }}
      className="modal"
      onClick={() => onClose()}
    >
      <div
        onClick={(e) => e.stopPropagation()}
        css={mq({
          width: ["300px", "300px", "500px"],
          justifyContent: "center",
          backgroundColor: "#fff",
          borderRadius: "10px",
          padding: "20px",
          transition: "all 0.3s ease-in-out",
        })}
      >
        {children}
      </div>
    </div>
  );
};

export default Modal;
