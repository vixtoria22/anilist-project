import {
  ApolloClient,
  InMemoryCache,
  gql,
} from "@apollo/client";

export const client = new ApolloClient({
  uri: "https://graphql.anilist.co/",
  cache: new InMemoryCache(),
});

export const GET_ANIME_LIST = gql`
  query ($page: Int, $perPage: Int) {
    Page(page: $page, perPage: $perPage) {
      pageInfo {
        total
        currentPage
        lastPage
        hasNextPage
        perPage
      }
      media {
        id
        title {
          romaji
        }
        coverImage {
          extraLarge
          large
        }
      }
    }
  }
`;

export const GET_ANIME_DETAIL = gql`
  query ($id: Int) {
    Media(id: $id) {
      id
      title {
        romaji
      }
      coverImage {
        extraLarge
        large
      }
      bannerImage
      description
      episodes
      genres
      averageScore
      meanScore
      status
      relations {
        edges {
          id
          node {
            id
            title {
              romaji
            }
            coverImage {
              extraLarge
              large
            }
          }
        }
      }
      startDate {
        year
        month
        day
      }
    }
  }
`;