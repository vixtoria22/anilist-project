import { iAnilistMedia } from "../interfaces/media";

const COLLECTION_LIST = "COLLECTION_LIST";
const COLLECTION_PREFIX = "COLLECTION_DATA_";

const ERROR_EMPTY = new Error("Collection Name should not be empty");
const ERROR_NON_ALPHA = new Error(
  "Collection Name should only contains alphanumeric characters"
);
const ERROR_NON_UNIQUE = new Error("Collection Name must be unique");

function isAlphaNumeric(str: string) {
  let code;
  let i = 0;

  while (i < str.length) {
    code = str.charCodeAt(i);
    if (
      !(code > 47 && code < 58) && // numeric (0-9)
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)
    ) {
      // lower alpha (a-z)
      return false;
    }
    i++;
  }
  return true;
}

export const getCollectionList = (): string[] => {
  const rawCollectionList = localStorage.getItem(COLLECTION_LIST);
  if (!rawCollectionList) return [];
  return JSON.parse(rawCollectionList);
};

export const addNewCollection = (collection: string): Error | void => {
  if (!collection) throw ERROR_EMPTY;
  if (!isAlphaNumeric(collection)) throw ERROR_NON_ALPHA;
  const collectionList = getCollectionList();
  const checkDuplicate = collectionList.find(
    (thisCollection) => collection === thisCollection
  );
  if (checkDuplicate) throw ERROR_NON_UNIQUE;
  collectionList.push(collection);
  localStorage.setItem(COLLECTION_LIST, JSON.stringify(collectionList));
  return;
};

export const addToCollection = (
  collection: string,
  anime: iAnilistMedia[]
): Error | void => {
  const withPrefix = `${COLLECTION_PREFIX}${collection}`;
  const getCollectionItems = getCollectionDetail(collection);
  let collectionItem: iAnilistMedia[] = [...getCollectionItems, ...anime];
  if (getCollectionItems.length) {
    if (anime.length > 1) {
      collectionItem = collectionItem.filter(
        ({ id }, index, thisAnime) =>
          thisAnime.findIndex((otherAnime) => id === otherAnime.id) === index
      );
    } else {
      const checkDuplicate = getCollectionItems.find(
        (thisAnime) => anime[0].id === thisAnime.id
      );
      if (checkDuplicate)
        throw new Error(
          `You already added this anime to ${collection} collection`
        );
      collectionItem = [...getCollectionItems, ...anime];
    }
  }

  localStorage.setItem(withPrefix, JSON.stringify(collectionItem));
  return;
};

export const getCollectionDetail = (collection: string): iAnilistMedia[] => {
  const withPrefix = `${COLLECTION_PREFIX}${collection}`;
  const rawCollectionItem = localStorage.getItem(withPrefix);
  if (!rawCollectionItem) return [];
  return JSON.parse(rawCollectionItem);
};

export const getCollectionsByAnime = (id: number): string[] => {
  const collectionList = getCollectionList();
  return collectionList.filter((thisCollection) =>
    getCollectionDetail(thisCollection).find((thisAnime) => thisAnime.id === id)
  );
};

export const deleteCollection = (collection: string) => {
  const withPrefix = `${COLLECTION_PREFIX}${collection}`;
  localStorage.removeItem(withPrefix);
  const collectionList = getCollectionList();
  collectionList.splice(
    collectionList.findIndex((thisCollection) => thisCollection === collection),
    1
  );
  localStorage.setItem(COLLECTION_LIST, JSON.stringify(collectionList));
  return;
};

export const editCollection = (collection: string, newCollection: string) => {
  if (newCollection === collection)
    throw new Error("New collection same as Old, nothing changed");
  if (!newCollection) throw ERROR_EMPTY;
  if (!isAlphaNumeric(newCollection)) throw ERROR_NON_ALPHA;
  const collectionList = getCollectionList();
  const foundOld = collectionList.find(
    (thisCollection) => thisCollection === collection
  );
  if (!foundOld) throw Error(`Collection ${collection} not found `);
  const foundNew = collectionList.find(
    (thisCollection) => thisCollection === newCollection
  );
  if (foundNew) throw Error(`You already have ${newCollection} collection`);

  const withPrefix = `${COLLECTION_PREFIX}${collection}`;
  const newWithPrefix = `${COLLECTION_PREFIX}${newCollection}`;
  const oldData = localStorage.getItem(withPrefix);
  localStorage.setItem(newWithPrefix, oldData || "");
  localStorage.removeItem(withPrefix);
  collectionList.splice(
    collectionList.findIndex((thisCollection) => thisCollection === collection),
    1,
    newCollection
  );
  localStorage.setItem(COLLECTION_LIST, JSON.stringify(collectionList));
  return;
};

export const deleteAnimeFromCollection = (collection: string, id: number) => {
  const withPrefix = `${COLLECTION_PREFIX}${collection}`;
  const detailCollection = getCollectionDetail(collection);
  detailCollection.splice(
    detailCollection.findIndex((thisAnime) => thisAnime.id === id),
    1
  );
  localStorage.setItem(withPrefix, JSON.stringify(detailCollection));
  return;
};
